# ReadMe

COMP-4476-WDE Cryptography Network Security

Ben Puhalski

**Assignment 3**

# Table of Contents

- [Problem 1](#problem-1)
  - [Round 1](#round-1)
  - [Round 2](#round-2)
  - [Round 3](#round-3)
  - [Final Hash Output](#final-hash-output)

- [Problem 2](#problem-2)

- [Problem 3](#problem-3)

- [Running the program](#Running-the-program)

# Problem 1

Code in ProblemOne.java

## Round 1

```java
out = new int[5];
Arrays.fill(out, 0);
```

Fills out array with zeros

```java
int[][][] blockText = new int[(int)(Math.ceil(s.length()/25.0))][5][5];

char[] strArr = s.toCharArray();

//index block num
for(int i = 0; i < (int)(Math.ceil(s.length()/25.0)); i++) {
    //index y of block
    for(int y = 0; y < 5; y++) {
        //index x
        for(int x = 0; x < 5; x++) {
            try {
                blockText[i][x][y] = charToInt_Z27(strArr[(i * 25) + (y * 5) + x]);
            } catch(IndexOutOfBoundsException ex) {
                blockText[i][x][y] = 0;
            }
        }
    }
}
```

Converts input string to array of 5x5 blocks of integers representing the characters in the input string

```java
for(int i = 0; i < 5 * (int)(Math.ceil(s.length()/25.0)); i++) {
    for(int j = 0; j < 5; j++) {
        out[i % 5] += blockText[(int)(Math.floor(i/5.0))][i % 5][j];
    }
}
for(int i = 0; i < out.length; i++) {
    out[i] %= 27;
}
```

add the columns of blockText to out[] then mod 5.

<img src="figures/round1.png"/>

## Round 2

Does the same operations as round 1 but adds...

```java
int[][][] shiftBlockText = new int[(int)(Math.ceil(s.length()/25.0))][5][5];
//index through shift block num
for(int i = 0; i < (int)(Math.ceil(s.length()/25.0)); i++) {
    //index y of block
    for(int y = 0; y < 5; y++) {
        //index x
        for(int x = 0; x < 5; x++) {
            if((x - (y+1)) < 0) {
                shiftBlockText[i][(x - (y + 1)) + 5][y] = blockText[i][x][y];
            } else {
                shiftBlockText[i][x - (y + 1)][y] = blockText[i][x][y];
            }
        }
    }
}
```

creates a new 3D array to represent all the blocks but shifted left by the ith row for i = 1,2,3,4,0.

After this it updates out[] with the columns of shiftBlockText.

<img src="figures/round2.png"/>

## Part 3

Does the same operations as round 1 and 2 but adds...

```java
int[][][] rowBlockText = new int[(int)(Math.ceil(s.length()/25.0))][5][5];
//index through shift block num
for(int i = 0; i < (int)(Math.ceil(s.length()/25.0)); i++) {
    //index x of block
    for(int x = 0; x < 5; x++) {
        //index x
        for(int y = 0; y < 5; y++) {
            if((y + (x+1)) > 4) {
                rowBlockText[i][x][(y + (x+1)) - 5] = round2Block[i][x][y];
            } else {
                rowBlockText[i][x][y + (x+1)] = round2Block[i][x][y];
            }
        }
    }
}
```

Creates a new 3D array to represent all the blocks but shifted left by the ith column for i = 1,2,3,4,0.

After this it updates out[] with the rows of rowBlockText.

<img src="figures/round3a.png"/>

## Final Hash Output

```java
clearOut();
round1(s);
round2(s);
round3(s);
String o = "";
for(int i = 0; i < out.length; i++) {
    o += intToChar_Z27(out[i]);
}
return o;
```

Clears out array, goes through operations round 1, 2 and 3, converts the characters in out to integers.

<img src="figures/round3b.png"/>

# Problem 2

Code used in ProblemTwo.java

---

```java
ProblemOne.hash(k + ProblemOne.hash(k + x));
```

k is the secret key and x is the plaintext. Calls the hash function from problem 1.

<img src="figures/problem2.png"/>

# Problem 3

Code used in ProblemThree.java

---

```java
public static ArrayList<String[]> bruteForce(int collisionNum) {

    HashMap<String, String> hashes = new HashMap<String, String>();
    ArrayList<String[]> cols = new ArrayList<String[]>();

    String conv;
    String s;
    int num = 1;
    int numberSearched = 0;
	//loop until program has found the number of collisions requested
    while(cols.size() < collisionNum) {
		//turns the incremental num to a number with base 28
        conv = baseConv(Integer.toString(num), 10, 28);
        char[] ar = conv.toCharArray();
		
        //converts the number to a string in Z27
        for(int i = 0; i < conv.length(); i++) {
            //System.out.println("lop " + ar[i]);
            if(ar[i] >= '0' && ar[i] <= '9') {
                ar[i] = (char) (ar[i] - '0' + 'a' - 1);
            } else if(ar[i] == 'r') {
                ar[i] = ' ';
            } else {
                ar[i] += 9;
            }
        }

        s = new String(ar);
        //System.out.println("entering " + s + "\t" + conv);

        //put the new string into the hashmap
        String b = hashes.put(ProblemOne.hash(s), s);

        //if the hash is not unique
        if(b != null) {
            String[] sArr = new String[3];
            sArr[0] = s;
            sArr[1] = b;
            sArr[2] = String.valueOf(numberSearched + 1);
            cols.add(sArr);
            //collect the new string, old string, number of plaintexts searched
        }

        //increment the number for string bruteforce
        num++;
        if(num % 28 == 0) { //so the order is ...y, z, _, aa, ab...
            num++; 			//instead of ...y, z, _, ba, bb...
        }

        //keep track of plaintexts searched
        numberSearched++;
    }
    
    return cols;
}
```

Using hashmap so the program can check for collisions easily while also retaining the original input message value. Increments using integers then converts that integer to a corresponding string with a base in Z27. Identifies collisions and adds them to an arraylist. Input 'collisionNum' specifies how many collisions the function will go through before terminating.

<img src="figures/problem3.png"/>

# Running the program

In the builds directory there is a cypto.jar file. Run the file like:

<img src="figures/buildOutput.png"/>

- java -jar CryptoAssignment3.jar 1

- java -jar CryptoAssignment3.jar 2

- java -jar CryptoAssignment3.jar 3

Send the jar an argument to run the corresponding problems code. (1, 2, or 3) 

