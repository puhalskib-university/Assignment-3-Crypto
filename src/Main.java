package com.puhalski;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        try {
            switch (args[0]) {
                case "1":
                    problem1();
                    break;
                case "2":
                    problem2();
                    break;
                case "3":
                    problem3();
                    break;
                default:
                    System.out.println("Run a program by passing an argument (1, 2, or 3");
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Run a program by passing an argument (1, 2, or 3)");
        }
    }

    public static void problem1() {
        //problem 1
        String s = "abcdefghi jklmnopqrstuvwx";
        System.out.println(s);

        //round 1
        ProblemOne.clearOut();
        System.out.println(ProblemOne.round1(s));
        System.out.println("out:\t" + Arrays.toString(ProblemOne.out));

        //rounds 1 and 2
        ProblemOne.clearOut();
        ProblemOne.round1(s);
        System.out.println(ProblemOne.round2(s));
        System.out.println("out:\t" + Arrays.toString(ProblemOne.out));

        //rounds 1, 2 and 3
        ProblemOne.clearOut();
        ProblemOne.round1(s);
        ProblemOne.round2(s);
        System.out.println(ProblemOne.round3(s));
        System.out.println("out:\t" + Arrays.toString(ProblemOne.out));

        //hash
        System.out.println(s + " => " + ProblemOne.hash(s));
        s = "the birthday attack can be performed for any hash functions including sha three";
        System.out.println(s + " => " + ProblemOne.hash(s));
    }
    public static void problem2() {
        String key = "aoydw";
        String x = "the birthday attack can be performed for any hash functions including sha three";
        System.out.println("Using key: " + key);
        System.out.println("Using plaintext: " + x);
        System.out.println("Mac: " + ProblemTwo.MAC(key, x));
    }
    public static void problem3() {

        ArrayList<String[]> ar = ProblemThree.bruteForce(4);
        for(int i = 0; i < ar.size(); i++) {
            System.out.println(ar.get(i)[0] + " and " + ar.get(i)[1] + " hashes match" + "\tHash: " + ProblemOne.hash(ar.get(i)[0]));
            System.out.println(ar.get(i)[2] + " plaintexts searched");
        }

    }
}
