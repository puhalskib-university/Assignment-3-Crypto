package com.puhalski;

import java.util.ArrayList;
import java.util.HashMap;

public class ProblemThree {

    public static ArrayList<String[]> bruteForce(int collisionNum) {
        HashMap<String, String> hashes = new HashMap<String, String>();

        ArrayList<String[]> cols = new ArrayList<String[]>();

        String conv;
        String s;
        int num = 1;
        int numberSearched = 0;

        while(cols.size() < collisionNum) {
            conv = baseConv(Integer.toString(num), 10, 28);
            char[] ar = conv.toCharArray();
            for(int i = 0; i < conv.length(); i++) {
                //System.out.println("lop " + ar[i]);
                if(ar[i] >= '0' && ar[i] <= '9') {
                    ar[i] = (char) (ar[i] - '0' + 'a' - 1);
                } else if(ar[i] == 'r') {
                    ar[i] = ' ';
                } else {
                    ar[i] += 9;
                }
            }
            s = new String(ar);
            //System.out.println("entering " + s + "\t" + conv);

            String b = hashes.put(ProblemOne.hash(s), s);
            if(b != null) {
                String[] sArr = new String[3];
                sArr[0] = s;
                sArr[1] = b;
                sArr[2] = String.valueOf(numberSearched + 1);

                cols.add(sArr);
            }

            num++;
            if(num % 28 == 0) {
                num++;
            }
            numberSearched++;
        }

        return cols;
    }

    //converts a number represented as a string to an int with a different base
    public static String baseConv(String num,int startBase, int endBase)
    {
        return Integer.toString(Integer.parseInt(num, startBase), endBase);
    }
}
