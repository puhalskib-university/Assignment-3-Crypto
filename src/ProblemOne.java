package com.puhalski;

import java.util.Arrays;

public class ProblemOne {

    //public variables
    public static int[] out;

    //private variables
    private static int[][][] round1Block;
    private static int[][][] round2Block;
    private static int[][][] round3Block;

    /*
     * Public methods
     */

    public static void clearOut() {
        out = new int[5];
        Arrays.fill(out, 0);
    }

    public static String round1(String s) {
        // block num                 5x5 block
        int[][][] blockText = new int[(int)(Math.ceil(s.length()/25.0))][5][5];

        char[] strArr = s.toCharArray();

        //index block num
        for(int i = 0; i < (int)(Math.ceil(s.length()/25.0)); i++) {
            //index y of block
            for(int y = 0; y < 5; y++) {
                //index x
                for(int x = 0; x < 5; x++) {
                    try {
                        blockText[i][x][y] = charToInt_Z27(strArr[(i * 25) + (y * 5) + x]);
                    } catch(IndexOutOfBoundsException ex) {
                        blockText[i][x][y] = 0;
                    }
                }
            }
        }

        //add columns to out
        for(int i = 0; i < 5 * (int)(Math.ceil(s.length()/25.0)); i++) {
            for(int j = 0; j < 5; j++) {
                out[i % 5] += blockText[(int)(Math.floor(i/5.0))][i % 5][j];
            }
        }
        for(int i = 0; i < out.length; i++) {
            out[i] %= 27;
        }

        round1Block = blockText;

        return blockToString(round1Block);
    }

    public static String round2(String s) {
        char[] strArr = s.toCharArray();

        int[][][] shiftBlockText = new int[(int)(Math.ceil(s.length()/25.0))][5][5];
        //index through shift block num
        for(int i = 0; i < (int)(Math.ceil(s.length()/25.0)); i++) {
            //index y of block
            for(int y = 0; y < 5; y++) {
                //index x
                for(int x = 0; x < 5; x++) {
                    if((x - (y+1)) < 0) {
                        shiftBlockText[i][(x - (y + 1)) + 5][y] = round1Block[i][x][y];
                    } else {
                        shiftBlockText[i][x - (y + 1)][y] = round1Block[i][x][y];
                    }
                }
            }
        }

        //add columns to out[]
        for(int i = 0; i < 5 * (int)(Math.ceil(s.length()/25.0)); i++) {
            for(int j = 0; j < 5; j++) {
                out[i%5] += shiftBlockText[(int)(Math.floor(i/5.0))][i % 5][j];
            }
        }
        for(int i = 0; i < out.length; i++) {
            out[i] %= 27;
        }


        round2Block = shiftBlockText;

        return blockToString(round2Block);
    }

    public static String round3(String s) {
        int[][][] rowBlockText = new int[(int)(Math.ceil(s.length()/25.0))][5][5];
        //index through shift block num
        for(int i = 0; i < (int)(Math.ceil(s.length()/25.0)); i++) {
            //index x of block
            for(int x = 0; x < 5; x++) {
                //index x
                for(int y = 0; y < 5; y++) {
                    if((y + (x+1)) > 4) {
                        rowBlockText[i][x][(y + (x+1)) - 5] = round2Block[i][x][y];
                    } else {
                        rowBlockText[i][x][y + (x+1)] = round2Block[i][x][y];
                    }
                }
            }
        }

        //add rows to out[]
        for(int i = 0; i < 5 * (int)(Math.ceil(s.length()/25.0)); i++) {
            for(int j = 0; j < 5; j++) {
                out[i%5] += rowBlockText[(int)(Math.floor(i/5.0))][j][i%5];
            }
        }
        for(int i = 0; i < out.length; i++) {
            out[i] %= 27;
        }


        round3Block = rowBlockText;

        return blockToString(round3Block);
    }

    public static String hash(String s) {
        clearOut();
        round1(s);
        round2(s);
        round3(s);
        String o = "";
        for(int i = 0; i < out.length; i++) {
            o += intToChar_Z27(out[i]);
        }
        return o;
    }

    /*
     * Private Methods
     */


    private static int charToInt_Z27(char c) {
        //convert to upper case
        if(c >= 'a' && c <= 'z') {
            c = (char) (c - 'a' + 'A');
        }
        // return corresponding integer
        if (c == ' ') {
            return 26;
        }
        //should be a letter
        if (c >= 'A' && c <= 'Z') {
            return c - 'A';
        } else {
            throw new IllegalStateException("Unexpected value: " + c);
        }
    }

    private static String blockToString(int[][][] a) {
        String p = "";
        for(int[][] b : a) {
            for(int j = 0; j < 5; j++) {
                for(int k = 0; k < 5; k++) {
                    p += b[k][j] + " ";
                    if(b[k][j] < 10) {
                        p += " ";
                    }
                }
                p += "\n";
            }
            p += "...\n";
        }
        return p;
    }

    private static char intToChar_Z27(int i) {
        // return corresponding integer
        if (i == 26) {
            return ' ';
        }
        //should be a letter
        if (i <= 25 && i >= 0) {
            return (char) (i + 'A');
        } else {
            throw new IllegalStateException("Unexpected value: " + i);
        }
    }



}
